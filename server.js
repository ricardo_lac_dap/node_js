var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
var port = process.env.PORT || 3000;

var baseMLabURL = "https://api.mlab.com/api/1/databases/apitechurld/collections/";
var requestJson = require('request-json');
var mLabAPIKey = "apiKey=7s4z0IvcBUFaFt24G3zOL-SINECwmzpf";

app.listen(port);
console.log("API escuchando en el puerto " + port);

app.get('/apitechu/v1',
function(req, res) {
  console.log("GET /apitechu/v1");
  res.send(
    {
      "msg" : "Bienvenido a la API tech U"
    }
  );
  }
);

app.get('/apitechu/v1/users',
  function(req, res){
    console.log("GET /apitechu/v1/users");
    res.sendFile('usuarios.json', {root:__dirname});
  }
)

app.get('/apitechu/v2/users',
  function(req, res){
    console.log("GET /apitechu/v2/users");

    httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente creado");

    httpClient.get("user?" + mLabAPIKey,
      function(err, resMLab, body) {
        var response = !err ? body : {
          "msg" : "Error obteniendo usuarios."
        }
        res.send(response);
      }
  )
  }
)

//inicio get accounts
app.get('/apitechu/v2/users/:userid/accounts', //filtrar po id
 function(req, res) {
   console.log("GET /apitechu/v2/users/:userid/accounts");

   var userid = req.params.userid;
   var query = 'q={"userid" : ' + userid + '}';

   httpClient = requestJson.createClient(baseMLabURL);
   console.log("Cliente creado");

   httpClient.get("account?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error obteniendo usuario."
         }
         res.status(500);
       } else {
         if (body.length > 0) {
           response = body;
         } else {
           response = {
             "msg" : "Usuario no encontrado."
           };
           res.status(404);
         }
       }
       res.send(response);
     }
   )
 }
)
//end id account






app.get('/apitechu/v2/users/:id', //filtrar po id
 function(req, res) {
   console.log("GET /apitechu/v2/users/:id");

   var id = req.params.id;
   var query = 'q={"id" : ' + id + '}';

   httpClient = requestJson.createClient(baseMLabURL);
   console.log("Cliente creado");

   httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error obteniendo usuario."
         }
         res.status(500);
       } else {
         if (body.length > 0) {
           response = body[0];
         } else {
           response = {
             "msg" : "Usuario no encontrado."
           };
           res.status(404);
         }
       }
       res.send(response);
     }
   )
 }
)

//funcion login http
app.post('/apitechu/v2/login',
function(req, res) {
  console.log("POST /apitechu/v2/login");
  var password = req.body.password;
  var email = req.body.email;
  var query = 'q={"email" : "' + email + '", "password" : "' + password + '"  }';
  //var putBody = '{"$unset":{"logged":true}}'; logout

    httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente creado");

    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
        if (err) {
          response = {
            "msg" : "Error obteniendo usuario."
          }
          res.status(500);
        } else {
        if (body.length > 0) {
        var putBody = '{"$set":{"logged":true}}'; //login
        var query = 'q={"id" : ' + body[0].id +'}';//query del put
        httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
          function(errPUT, resMLabPUT, bodyPUT){
          }
        )
        response = {
          "msg" : "Usuario logado",
           "body" : body[0].id,
          "bodylength" : body.length
        };
          } else {
          response = {
            "msg" : "Usuario no encontrado."
          };
         res.status(404);
        }
        }
        res.send(response);
      }
    )
}
)
//end funcion login http

//function logout http
app.post('/apitechu/v2/logout/:id',
 function(req, res) {
   console.log("POST /apitechu/v2/logout/:id");

   var query = 'q={"id": ' + req.params.id + '}';
   console.log("query es " + query);

   httpClient = requestJson.createClient(baseMLabURL);
   httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (body.length == 0) {
         var response = {
           "mensaje" : "Logout incorrecto, usuario no encontrado"
         }
         res.send(response);
       } else {
         console.log("Got a user with that id, logging out");
         query = 'q={"id" : ' + body[0].id +'}';
         console.log("Query for put is " + query);
         var putBody = '{"$unset":{"logged":""}}'
         httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             console.log("PUT done");
             var response = {
               "msg" : "Usuario deslogado con éxito",
               "idUsuario" : body[0].id
             }
             res.send(response);
           }
         )
       }
     }
   );
 }
)
//end function logout http


app.post('/apitechu/v1/users',
  function(req, res){
  console.log("POST /apitechu/v1/users");
  //console.log(req);
  console.log("first_name es " + req.body.first_name);
  console.log("last_name es " + req.body.last_name);
  console.log("country es " + req.body.country);

  var newUser = { //objeto
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "country": req.body.country
    };

  var users = require('./usuarios.json');
  users.push(newUser);
  writeUserDataToFile(users);
  //console.log("Usuario añadido con exito");
//  res.send(users);

res.send(
    {
        "msg" : "Usuario añadido con exito"
      }
  );
  }
)

app.delete("/apitechu/v1/users/:id",
  function(req, res) {
    console.log("DELETE /apitechu/v1/users/:id");
    var users = require('./usuarios.json');
    users.splice(req.params.id - 1, 1); //trozos que quita
    writeUserDataToFile(users);
    res.send(
      {
        "msg" : "usuario borrado con exito"
      }
    );
  }
)

function writeUserDataToFile(data) {
var fs = require('fs');
var jsonUserData = JSON.stringify(data);

fs.writeFile(
  "./usuarios.json",
  jsonUserData,
  "utf8",
  function(err){
    if (err){
      console.log(err);
    } else {
      console.log("Fichero de Usuario persistido");
    }
  }
)
}

//nueva funcion login//
app.post('/apitechu/v1/login',
  function(req, res){
    var loginUserEmail = { //objeto
      "email": req.body.email
      };
      var loginUserPassword = { //objeto
        "password": req.body.password
        };
    console.log("llamada login ok");
    console.log("email es " + req.body.email);
    console.log("password es " + req.body.password);
    var users = require("./usuarios.json");
for (user of users){
    if ((user['email'] == loginUserEmail['email']) && (user['password'] == loginUserPassword['password']) ) {
      user['logged'] = true;
      console.log(user['logged']);
      res.send(
                {
              "msg" : "loging correcto",
              "id" :user['id']
              }
          );
    }
}// acaba el loop

for (user of users){
    if ((user['email'] != loginUserEmail['email']) || (user['password'] != loginUserPassword['password']) ) {
      res.send(
            {
              "msg" : "login incorrecto"
              }
          );
    }
}// acaba el loop

    }
    )

    //nueva funcion logout//
    app.post('/apitechu/v1/logout',
      function(req, res){
        var loginUserId = { //objeto
          "id": req.body.id
          };
        var users = require("./usuarios.json");
        for (user of users){
          if (user['id'] == loginUserId['id']){
            delete user['logged'];
            res.send(
                      {
                    "msg" : "logout correcto",
                    "id" :user['id']
                    }
                );
          }
        } //end loop
        for (user of users){
          if (user['id'] != loginUserId['id']){
            res.send(
                      {
                    "msg" : "logout incorrecto"
                    }
                );
          }
        } //end loop

      }
    )








app.post('/apitechu/v1/monstruo/:p1/:p2',
  function(req, res) {//prueba envio de front a back
    console.log("Parametros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
  }
);
